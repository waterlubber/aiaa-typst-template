#import "aiaa_template.typ": *

#show: aiaa_template.with(
  title: "Example Title",
  authors: (
    (
      name: "First A. Author",
      affiliation: "Business or Academic Affiliation’s Full Name 1, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for first author."
    ),
    (
      name: "Second A. Author, Jr.",
      affiliation: "Business or Academic Affiliation’s Full Name 1, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for second author."
    ),
    (
      name: "Fourth D. Author",
      affiliation: "Business or Academic Affiliation’s Full Name 2, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for fourth author."
    ),
    (
      name: "Third C. Author",
      affiliation: "Business or Academic Affiliation’s Full Name 1, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for third author."
    ),
  ),
  abstract: [#lorem(45)]
)

// We generated the example code below so you can see how
// your document will look. Go ahead and replace it with
// your own content!

= Introduction
#lorem(60)

= Body Paragraphs
#lorem(20)
== Subheading Example
#lorem(15) Footnote example 1! #footnote()[Bottom Text] #lorem(10)

Example Text followed by block quote: #lorem(20) The block quote is from @hitchhikers. 
#quote("The ships hung in the sky in much the same way that bricks don't.")

#lorem(25)


#lorem(15)!#footnote()[Another example footnote#footnote("Unfortunately, footnote numbering with many authors is a little strange.")]

== Second Subheading
#lorem(10) Inline equation $p = 1/2 rho v^2$ followed by separate equation @eq1. 
$ sqrt(a^2 + b^2) = c $<eq1>

=== Example Figures and Tables
Here's an example figure, @fig1:
#figure(caption: "This is the caption of the figure.",box(fill: luma(200), height: 1in + 1em, inset: 0.5in)[This is the content of the figure]) <fig1>

Likewise, here's an example table, @table1:
#figure(caption: "Here's an example table.", table(
  columns: (0.5fr, 0.25fr, 0.25fr),
  inset: 10pt,
  align: horizon,
  [], [*Area*], [*Parameters*],
  "Cylinder",
  $ pi h (D^2 - d^2) / 4 $,
  [
    $h$: height \
    $D$: outer radius \
    $d$: inner radius
  ],
  "Tetrahedron",
  $ sqrt(2) / 12 a^3 $,
  [$a$: edge length]
))<table1>

Finally, some concluding text under the table.

=== Tertiary Heading Level
#lorem(15)
=== Continuing Tertiary Heading
- List of items
- List of items, item 2
- Etc.

== Big 2
#lorem(25)

#lorem(25)

= Related Work
#lorem(50)

#lorem(50)

#bibliography(full: true, "bib.yml")
