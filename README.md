# AIAA Typst Template
This is a template for the American Institute of Aeronautics and Astronautics paper format for [Typst](https://typst.app). 
It is still a work in progress, as it contains many workarounds for Typst's current quirks.

Basic usage:
```typst
#import "aiaa_template.typ": *

#show: aiaa_template.with(
  title: "Example Title",
  authors: (
    (
      name: "First A. Author",
      affiliation: "Business or Academic Affiliation’s Full Name 1, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for first author."
    ),
    (
      name: "Second A. Author, Jr.",
      affiliation: "Business or Academic Affiliation’s Full Name 1, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for second author."
    ),
    (
      name: "Fourth D. Author",
      affiliation: "Business or Academic Affiliation’s Full Name 2, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for fourth author."
    ),
    (
      name: "Third C. Author",
      affiliation: "Business or Academic Affiliation’s Full Name 1, City, State, Zip Code, Country",
      job: "Insert Job Title, Department Name, and AIAA Member Grade (if any) for third author."
    ),
  ),
  abstract: [#lorem(45)]
)

= Introduction
Write the main body of your paper here.

#bibliography(full: true, "bib.yml")
``` 

Headings, figures, tables, etc. all work. Note that there are some still some lingering indentation issues; for paragraphs immediately
following a figure or figure caption, you may need to manually indent with `#h(0.2in)`. 
